import os
import gzip
import csv
import itertools

import tqdm
import psycopg


def gen_rows(limit=None):
    with open("../adresses-france.csv", "r") as f:
        dialect = csv.excel
        dialect.delimiter = ";"
        reader = csv.DictReader(f, dialect=dialect)

        for _, row in zip(
            range(limit) if limit is not None else itertools.count(), reader
        ):
            if len(row["code_insee"]) <= 5:
                yield [row[k] for k in ("code_insee", "lon", "lat")]


def main():
    uri = os.getenv("PG_API0057_URI_RW")
    with psycopg.connect(uri, row_factory=psycopg.rows.dict_row) as conn:
        with conn.cursor() as cur:
            cur.execute("""DROP TABLE IF EXISTS adresses;""")
            cur.execute(
                """CREATE TABLE adresses (code_insee VARCHAR(5), lon FLOAT, lat FLOAT);"""
            )

            with cur.copy("COPY adresses (code_insee, lon, lat) FROM STDIN;") as writer:
                for row in tqdm.tqdm(gen_rows(), unit_scale=True):
                    writer.write_row(row)
        conn.commit()


if __name__ == "__main__":
    main()
