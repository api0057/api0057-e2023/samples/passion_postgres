Passion Postgres
================

* Clone the repository
* Run `make init`
* Download the ban address database as a CSV

```bash
wget https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-france.csv.gz
```

* Run the `__main__.py` script with: `python .`